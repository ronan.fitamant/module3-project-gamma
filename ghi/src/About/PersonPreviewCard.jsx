import { useNavigate } from "react-router-dom";

export default function PersonPreviewCard({ props, imageURL, name }) {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate("/about");
  };

  return (
    <div {...props} className="person-preview">
      <img
        onClick={handleClick}
        className="profile-pic preview-pic"
        src={imageURL}
        alt={name}
      />
      <span onClick={handleClick}>{name}</span>
    </div>
  );
}
