import { NavLink, useNavigate } from "react-router-dom";
import { SearchBar } from "./Search";
import { useGetAccountQuery, useLogoutMutation } from "../app/apiSlice";

export default function Nav() {
  const { data: account } = useGetAccountQuery();
  const navigate = useNavigate();
  const [logout] = useLogoutMutation();
  return (
    <nav className="navbar navbar-expand-lg overlay">
      <div className="container-fluid flex-nowrap">
        <NavLink className={"navbar-brand"} to="/">
          HOME
        </NavLink>
        <NavLink className={"navbar-brand"} to="anime/list">
          ANIME
        </NavLink>
        <NavLink className={"navbar-brand"} to="/random">
          RANDOM
        </NavLink>
        <SearchBar />
        {account && (
          <NavLink to={"watchlist"} className={"navbar-brand"}>
            WATCHLIST
          </NavLink>
        )}

        {!account && (
          <NavLink to={"/login"} className={"navbar-brand"}>
            LOGIN
          </NavLink>
        )}
        {!account && (
          <NavLink to={"/signup"} className={"navbar-brand"}>
            SIGN UP
          </NavLink>
        )}

        {account && <h5 className="greeting">Hi, {account.full_name}!</h5>}
        {account && (
          <button
            className="btn btn-outline-danger logout"
            onClick={() => {
              logout();
              navigate("/");
            }}
          >
            LOGOUT
          </button>
        )}
      </div>
    </nav>
  );
}
