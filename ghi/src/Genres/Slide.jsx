import { Link } from "react-router-dom";
import useTilt from "./useTilt";

export default function Slide({ slide, offset, props }) {
  const active = offset === 0 ? true : null;
  const ref = useTilt(active);

  // eslint-disable-next-line no-unused-vars
  const title = {
    textAlign: "center",
  };

  return (
    <div
      className={`slide ${Math.abs(offset) > 2 ? "d-none" : ""}`}
      {...props}
      ref={active ? ref : null}
      data-active={active}
      style={{
        "--offset": offset,
        "--dir": offset === 0 ? 0 : offset > 0 ? 1 : -1,
        backgroundImage: `url('${slide.image}')`,
      }}
    >
      <h3 className="slide-title">{slide.title}</h3>
      <Link to={`anime/list/${slide.title}`}>
        <button className="genre-button"></button>
      </Link>
    </div>
  );
}
