import AnimeCard from "../Anime/AnimeCard";
import { useGetWatchlistQuery } from "../app/apiSlice";
import { useEffect } from "react";

const WatchlistPage = () => {
  const { data: watchlist, isLoading, refetch } = useGetWatchlistQuery();

  useEffect(() => {
    refetch();
  }, [refetch]);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="watchlist-grid-ultimate">
      <div>
        <h1 style={{ textIndent: "3vw" }}>My Watchlist</h1>
        <div className="divider"></div>
        <div className="space"></div>
      </div>

      <div className="row mt-3 watchlist-grid">
        {watchlist?.map((anime) => (
          <AnimeCard key={anime.anime.anime_id} anime={anime.anime} />
        ))}
      </div>
    </div>
  );
};

export default WatchlistPage;
