import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";

import MainPage from "./MainPage.js";
import Nav from "./Nav/Nav.js";
import Footer from "./Nav/Footer";
import Random from "./Anime/Random";
import AnimeDetail from "./Anime/AnimeDetail";
import LoginPage from "./Auth/LoginPage";
import About from "./About/About";
import AnimeList from "./Anime/AnimeList";
import SignUp from "./Auth/SignUp";
import Watchlist from "./Watchlist/Watchlist";
import PrivacyPolicy from "./Misc/PrivacyPolicy";
import TermsAndConditions from "./Misc/TermsAndConditions";
import ProtectedRoute from "./Auth/ProtectedRoute";
import GenreList from "./Anime/GenreList";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');
  return (
    <BrowserRouter basename={basename}>
      <Nav />
      <div className="container-fluid">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="random" element={<Random />} />
          <Route path="anime/:anime_id" element={<ProtectedRoute element={<AnimeDetail/>}/>} />
          <Route path="login" element={<LoginPage />} />
          <Route path="login/:path" element={<LoginPage />} />
          <Route path="about" element={<About />} />
          <Route path="anime/list" element={<AnimeList />} />
          <Route path="signup" element={<SignUp />} />
          <Route path="watchlist" element={<ProtectedRoute element={<Watchlist/>}/>} />
          <Route path="privacy" element={<PrivacyPolicy />} />
          <Route path="terms" element={<TermsAndConditions />} />
          <Route path="anime/list/:genre" element={<GenreList />} />
        </Routes>
      </div>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
