import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const animeApi = createApi({
  reducerPath: "animeApi",
  baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_API_HOST }),
  endpoints: (builder) => ({
    getGenres:builder.query({
      query: () => ({
        url: `api/anime/genres`
      }),
      transformResponse: (response) => response?.genres,
      providesTags: ["Genre"]
    }),
    getRandime: builder.query({
      query: (params) =>   `/api/anime/random?` +
      `${params?.search ? `search=${params.search}`: ''}` +
      `${params?.type ? `&type=${params.type}` : ''}` +
      `${params?.page ? `&page=${params.page}`: ''}` +
      `${params?.min_score ? `&min_score=${params.min_score}`: ''}` +
      `${params?.max_score ? `&max_score=${params.max_score}`: ''}` +
      `${params?.status ? `&status=${params.status}`: ''}` +
      `${params?.rating ? `&rating=${params.rating}`: ''}` +
      `${params?.genres ? `&genres=${params.genres}`: ''}` +
      `${params?.genres_exclude ? `&genres_exclude=${params.genres_exclude}`: ''}` +
      `${params?.order_by ? `&order_by=${params.order_by}`: ''}` +
      `${params?.sort ? `&sort=${params.sort}`: ''}` +
      `${params?.start_date ? `&start_date=${params.start_date}`: ''}` +
      `${params?.end_date ? `&end_date=${params.end_date}`: ''}`,
    }),
    getAnime: builder.query({
      query: (params) =>   `/api/anime?` +
      `${params?.search ? `search=${params.search}`: ''}` +
      `${params?.type ? `&type=${params.type}` : ''}` +
      `${params?.page ? `&page=${params.page}`: ''}` +
      `${params?.min_score ? `&min_score=${params.min_score}`: ''}` +
      `${params?.max_score ? `&max_score=${params.max_score}`: ''}` +
      `${params?.status ? `&status=${params.status}`: ''}` +
      `${params?.rating ? `&rating=${params.rating}`: ''}` +
      `${params?.genres ? `&genres=${params.genres}`: ''}` +
      `${params?.genres_exclude ? `&genres_exclude=${params.genres_exclude}`: ''}` +
      `${params?.order_by ? `&order_by=${params.order_by}`: ''}` +
      `${params?.sort ? `&sort=${params.sort}`: ''}` +
      `${params?.start_date ? `&start_date=${params.start_date}`: ''}` +
      `${params?.end_date ? `&end_date=${params.end_date}`: ''}`,
      providesTags: ['Anime'],
    }),
    getOneAnime: builder.query({
      query: (anime_id)=> ({
        url: `/api/anime/get/${anime_id}`,
        credentials: "include",
      }),
      transformResponse: (response) => response?.anime[0],
      providesTags: ['Anime'],
    }),
    getWatchlist: builder.query({
      query: () => ({
        url: '/api/watchlist',
        credentials: "include",
      }),
      transformResponse: (response) => response?.watchlist || null,
      providesTags:["Watchlist"],
    }),
    addWatchlistItem: builder.mutation({
      query: (body) => ({
        url: '/api/watchlist',
        method: 'POST',
        body,
        credentials: "include",
      }),
      invalidatesTags:["Watchlist"],
    }),
    deleteWatchlistItem: builder.mutation({
      query: (body) => ({
        url: '/api/watchlist',
        method: "DELETE",
        body,
        credentials: "include",
      }),
      invalidatesTags:["Watchlist"],
    }),
    getAccount: builder.query({
      query: () => ({
        url: `/token`,
        credentials: "include",
      }),
      transformResponse: (response) => response?.account || null,
      providesTags: ["Account"],
    }),
    logout: builder.mutation({
      query: () => ({
        url: `/token`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Account", "Watchlist"],
    }),
    login: builder.mutation({
      query: ({ username, password }) => {
        const body = new FormData();
        body.append("username", username);
        body.append("password", password);
        return {
          url: `/token`,
          method: "POST",
          body,
          credentials: "include",
        };
      },
      invalidatesTags: ["Account","Watchlist"],
    }),
    signup: builder.mutation({
      query: (body) => ({
          url: `/api/accounts`,
          method: "POST",
          body,
          credentials: "include"
      }),
      invalidatesTags: ["Account"],
    }),
  }),
});

export const {
  useGetAnimeQuery,
  useGetOneAnimeQuery,
  useGetAccountQuery,
  useLoginMutation,
  useLogoutMutation,
  useSignupMutation,
  useGetWatchlistQuery,
  useAddWatchlistItemMutation,
  useDeleteWatchlistItemMutation,
  useGetGenresQuery,
  useGetRandimeQuery,
} = animeApi
