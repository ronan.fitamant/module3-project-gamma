import { configureStore } from '@reduxjs/toolkit'
import { animeApi } from './apiSlice'
import filterReducer from './filterSlice'

const store = configureStore({
  reducer: {
    filter: filterReducer,
    [animeApi.reducerPath]: animeApi.reducer,
  },

  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(animeApi.middleware)
})

export default store;
