import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

export default function Checkbox({ name, options, setFilterCriteria }) {
  const [selectedGenres, setSelectedGenres] = useState([]);
  const searchCriteria = useSelector((state) => state.filter.value);

  const handleChange = (e) => {
    const { value, checked } = e.target;
    if (checked && !selectedGenres.includes(`${value}`)) {
      setSelectedGenres((prevSelectedGenres) => [
        ...prevSelectedGenres,
        (value),
      ]);
    } else if (!checked) {
      setSelectedGenres((prevSelectedGenres) => {
        const newSelectedGenres = new Set(prevSelectedGenres);
        newSelectedGenres.delete((value));
        return [...newSelectedGenres];
      });
    }
  };
  useEffect(() => {
    setSelectedGenres(() => {
      if (searchCriteria[name]) {
        return [...searchCriteria[name].toString().split(",")]
      } else {
        return []
      }
      })
  }, [setSelectedGenres, name, searchCriteria])


  useEffect(() => {
    setFilterCriteria((prevFormData) => ({
      ...prevFormData,
      [name]: selectedGenres.join(","),
    }));
  }, [name, selectedGenres, setFilterCriteria]);

  return (
    <fieldset>
      {options.map((option) => (
        <div key={option.id}>
          <input
            type="checkbox"
            name={name}
            value={option.id}
            checked={selectedGenres.includes((option.id).toString())}
            onChange={handleChange}
          />
          <label htmlFor={option.id}>{option.name.toUpperCase()}</label>
        </div>
      ))}
    </fieldset>
  );
}
