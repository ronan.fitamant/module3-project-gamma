import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

export default function Radio({ name, func, options, setFilterCriteria }) {
  const searchCriteria = useSelector((state) => state.filter.value);
  const [selected, setSelected] = useState();

  const handleChange = (e) => {
    const { value, checked } = e.target
    if (checked && value !== selected) {
      setSelected(value)
    } else if (value === selected) {
      setSelected(null)
    }
  }

  useEffect(() => {
    setFilterCriteria((prevfilterCriteria) => ({
      ...prevfilterCriteria,
      [name] :selected,
    }));
  }, [selected, setFilterCriteria, name])

  useEffect(() => {
    setSelected(searchCriteria[name])
  }, [setSelected, searchCriteria, name])

  return (
    <div>
      <fieldset>
        {options.map((option) => (
          <div key={option}>
            <input
              type="radio"
              checked={option === selected}
              name={name}
              value={option}
              onClick={handleChange}
              onChange={func}
            />
            <label htmlFor={option}>{option.toUpperCase()}</label>
          </div>
        ))}
      </fieldset>
    </div>
  );
}
