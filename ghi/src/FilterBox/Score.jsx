import { useState } from "react";

export default function Score({ name, func, value = 5 }) {
  const [scoreValue, setScoreValue] = useState(value);

  const handleChange = (e) => {
    const { value } = e.target;
    setScoreValue(value);
    func(e);
  };

  return (
    <div>
      <p>
        {name.split("_").join(" ")}: {scoreValue}
      </p>
      <input
        type="range"
        name={name}
        min="1"
        max="10"
        value={scoreValue}
        onChange={handleChange}
      />
      <label htmlFor={name}>{name}</label>
    </div>
  );
}
