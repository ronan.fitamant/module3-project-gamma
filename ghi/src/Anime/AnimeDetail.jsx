import React from "react";
import { useParams } from "react-router-dom";
import { useGetAccountQuery, useGetOneAnimeQuery } from "../app/apiSlice";
import WatchlistButton from "../Watchlist/WatchListButton";

export default function AnimeDetail() {
  const { anime_id } = useParams();
  const { data, isLoading } = useGetOneAnimeQuery(anime_id);
  const { data: account } = useGetAccountQuery();

  if (isLoading) return <div>Loading...</div>;

  return (
    <div className="anime-detail-div">
      <div className="card-title-2">
        <h2>{data.title.english}</h2>
        <h4 style={{ fontStyle: "italic" }}>{data.title.japanese}</h4>
      </div>

      <div className="info-grid">
        <div className="image-container">
          <img src={data.large_image} alt={data.title.english} />
          <div>
            {account && (
              <WatchlistButton
                className="btn watchlist-button move-button"
                removeClass="move-remove-button"
                anime_id={Number(anime_id)}
              />
            )}
          </div>
        </div>
        <div className="score-rank-grid">
          <div className="score-rank">
            <div className="score-rank-div">
              <span>Score {data.score}</span>
              <span>Rank #{data.rank}</span>
            </div>
            <div className="anime-detail-divider"></div>
            <div className="anime-detail-text">
              <ul>
                <li>
                  <strong>Type: </strong>
                  {data.type}
                </li>
                <li>
                  <strong>Episodes: </strong>
                  {data.episodes}
                </li>
                <li>
                  <strong>Season: </strong>
                  {data.season}
                </li>
                <li>
                  <strong>Status: </strong>
                  {data.status}
                </li>
                <li>
                  <strong>Genre: </strong>
                  {data.genre.map((genre) => `${genre.name}`).join(", ")}
                </li>
                <li>
                  <strong>Studio: </strong>
                  {data.studio.name}
                </li>
                <li>
                  <strong>Duration: </strong>
                  {data.duration}
                </li>
                <li>
                  <strong>Year: </strong>
                  {data.year}
                </li>
                <li>
                  <strong>Rating: </strong>
                  {data.rating}
                </li>
              </ul>
            </div>
          </div>
          <div className="synopsis-div">
            <div>
              <h5 style={{ fontSize: "1.2vw" }}>
                <strong>Synopsis</strong>
              </h5>
              <div className="anime-detail-divider-2"></div>
              <p>{data.synopsis}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
