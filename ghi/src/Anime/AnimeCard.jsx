import { useGetAccountQuery } from "../app/apiSlice";
import { Link } from "react-router-dom";
import WatchlistButton from "../Watchlist/WatchListButton";

export default function AnimeCard({ props, anime }) {
  const { data: account } = useGetAccountQuery();

  return (
    <div className="card anime-list-card" {...props}>
      <img className="card-img" src={anime?.large_image} alt="" />
      {account && (
        <Link
          className="card-img-overlay card-link"
          to={`/anime/${anime.anime_id}`}
        ></Link>
      )}
      <div className="card-img-overlay">
        {account && (
          <WatchlistButton
            className="btn watchlist-button"
            anime_id={anime.anime_id}
          />
        )}
      </div>
      <div className="card-img-overlay">
        <h6 className="card-title">{anime?.title?.english}</h6>
      </div>
    </div>
  );
}
