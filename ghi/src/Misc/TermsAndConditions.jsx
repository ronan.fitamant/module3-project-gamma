export default function TermsAndConditions() {
  return (
    <div className="terms-and-conditions">
      <h1>Terms and Conditions of Use for Randime Website</h1>
      <p>Effective Date: 08/01/2023</p>

      <p>
        Please read these Terms and Conditions ("Terms") carefully before using
        the Randime website (the "Website") operated by Randime ("we," "us," or
        "our"). These Terms govern your use of the Website, and by accessing or
        using the Website, you agree to be bound by these Terms. If you do not
        agree with any part of these Terms, you should not use the Website.
      </p>

      <h2>1. Use of the Website:</h2>
      <p>
        <strong>1.1. Eligibility:</strong> By using Randime, you represent and
        warrant that you are at least 18 years old and have the legal capacity
        to enter into these Terms.
      </p>

      <p>
        <strong>1.2. User Account:</strong> Certain areas of Randime may require
        you to create a user account. You are responsible for maintaining the
        confidentiality of your account information, including your username and
        password. You agree to accept responsibility for all activities that
        occur under your account.
      </p>

      <p>
        <strong>1.3. Prohibited Activities:</strong> When using Randime, you
        agree not to:
        <ul>
          <li>
            Violate any applicable laws, regulations, or third-party rights;
          </li>
          <li>
            Engage in any unauthorized use, copying, or distribution of any
            content found on Randime;
          </li>
          <li>
            Attempt to gain unauthorized access to any portion of Randime or any
            related systems or networks;
          </li>
          <li>
            Introduce any viruses, malware, or other harmful code to Randime;
          </li>
          <li>
            Interfere with the proper functioning of Randime or compromise its
            security;
          </li>
          <li>
            Engage in any conduct that may disrupt, disable, or negatively
            affect Randime's services or users.
          </li>
        </ul>
      </p>

      <h2>2. Intellectual Property:</h2>
      <p>
        <strong>2.1. Ownership:</strong> Randime and its entire content,
        including but not limited to text, graphics, logos, images, software,
        and audiovisual materials, are the intellectual property of Randime or
        its licensors and are protected by applicable copyright, trademark, and
        other intellectual property laws.
      </p>

      <p>
        <strong>2.2. License:</strong> Subject to your compliance with these
        Terms, we grant you a limited, non-exclusive, non-transferable, and
        revocable license to access and use Randime for personal, non-commercial
        purposes.
      </p>

      <p>
        <strong>2.3. Restrictions:</strong> You are not permitted to:
        <ul>
          <li>
            Modify, adapt, translate, or create derivative works of Randime or
            its content;
          </li>
          <li>
            Remove, alter, or obscure any copyright, trademark, or other
            proprietary notices on the Website or its content;
          </li>
          <li>
            Use the Website for any commercial purpose without our prior written
            consent.
          </li>
        </ul>
      </p>

      <h2>3. Disclaimer of Warranties:</h2>
      <p>
        The Website and its content are provided on an "as is" and "as
        available" basis, without any warranties of any kind, express or
        implied. We do not guarantee that the Website will be error-free,
        secure, or continuously available. Your use of the Website is at your
        own risk.
      </p>

      <h2>4. Limitation of Liability:</h2>
      <p>
        In no event shall Randime be liable for any indirect, incidental,
        special, consequential, or punitive damages arising out of or in
        connection with your use of the Website or any content therein. We shall
        not be liable for any damages, losses, or expenses resulting from
        unauthorized access to or alteration of your transmissions or data.
      </p>

      <h2>5. Indemnification:</h2>
      <p>
        You agree to indemnify and hold Randime, its officers, directors,
        employees, and agents harmless from and against any and all claims,
        liabilities, damages, losses, costs, and expenses (including reasonable
        attorney's fees) arising out of or in connection with your use of the
        Website or any violation of these Terms.
      </p>

      <h2>6. Third-Party Links:</h2>
      <p>
        The Website may contain links to third-party websites or services that
        are not owned or controlled by Randime. We have no control over and
        assume no responsibility for the content, privacy policies, or practices
        of any third-party websites or services. Your interactions with
        third-party websites are solely between you and the respective third
        party.
      </p>

      <h2>7. Modifications to the Terms:</h2>
      <p>
        We reserve the right to modify these Terms at any time, and any changes
        will be effective when posted on this page. Your continued use of the
        Website after any such changes constitutes your acceptance of the
        revised Terms.
      </p>

      <h2>8. Governing Law and Jurisdiction:</h2>
      <p>
        These Terms shall be governed by and construed in accordance with the
        laws of your Country/State, without regard to its conflict of law
        principles. Any disputes arising under or in connection with these Terms
        shall be subject to the exclusive jurisdiction of the courts in [Your
        Country/State].
      </p>

      <h2>9. Contact Us:</h2>
      <p>
        If you have any questions or concerns about these Terms or the Website,
        please contact us at contact@randime.com.
      </p>

      <p>
        By using the Randime Website, you acknowledge that you have read,
        understood, and agree to be bound by these Terms and all applicable laws
        and regulations.
      </p>
    </div>
  );
}
