import { useGetAccountQuery } from "../app/apiSlice";
import { Navigate } from "react-router-dom";
import { useLocation } from "react-router-dom";

export default function ProtectedRoute({element}){
    const {data:account} = useGetAccountQuery();
    const location = useLocation();

    if(account){
        return (
            {...element}
        )
    }
    else {
        return(
           <Navigate to={`/login/${location.pathname.split('/').slice(1).join(',')}`} replace />
        )
    }
}
