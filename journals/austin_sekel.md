# 6/26/23
# Foundation Laid
# We modified the docker-compose.yaml file to match what we needed
# Backend configured & working
# Routers & queries configured

# 6/27/23
# Created endpoints for our watchlist
# Started working on connecting api for anime list

# 6/28/23
# Created endpoint from external api for the randomizer

# 6/29/23
# finished creating the essential things for the backend
# started messing around with react

# 6/30/23
# Launched the front page, created a nav & footer & started login process

# 7/10/23
# introduced redux to the project for the first time

# 7/11/23
# fixed up the search bar & got rid of the reset button but it's still not completely working, we're close though. # then we worked on the login functionality. it works at the bare minimum, so we started working on the
# sign up page together.

# 7/12/23
# we got the sign up page to work which is awesome, john is a coding wiz.

# 7/13/23
# absent

# 7/14/23
# absent

# 7/17/23
# added a YearSlider, ScoreSlider & checkbox. filterbox almost complete too

# 7/18/23
# unit test completed, got the anime list page to work

# 7/19/23
# finished creating tests, tried to get carousel images working, started using css

# 7/20/23
# fixed bugs. Created add_to_watchlist unit test

# 7/24/23
# got unit tests to work, added some styling with css, like border-radius, add social media profiles in footer

# 7/25/23
# finished filterbox, added some border-radius to round images & more styling

# 7/26/23
# nearly finished with anime list page, worked on anime card & the about
# preview. teammate images are up with links

# 7/27/23
# worked on making filterbox look better, remove from watchlist button

# 7/28/23
# tidy'd up everything according to the rubrics standard, we're ready
