from fastapi import APIRouter, Depends, HTTPException, status
from authenticator import authenticator
from queries.accounts import AccountOut, UserNotLoggedInError
from queries.anime import AnimeQueries
from queries.watchlist import (
    DuplicateWatchlistItem,
    WatchlistItemOut,
    WatchlistItemIn,
    WatchlistQueries,
    Watchlist,
)


router = APIRouter()


@router.get("/api/watchlist", response_model=Watchlist)
def get_watchlists(
    watchlist: WatchlistQueries = Depends(),
    user_data: AccountOut = Depends(
        authenticator.try_get_current_account_data
    ),
):
    try:
        return watchlist.get_watchlist(email=user_data["email"])
    except TypeError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="User not logged in",
        )


@router.post("/api/watchlist", response_model=WatchlistItemOut)
def add_to_watchlist(
    info: WatchlistItemIn,
    watchlist: WatchlistQueries = Depends(),
    queries: AnimeQueries = Depends(),
    user_data: AccountOut = Depends(
        authenticator.try_get_current_account_data
    ),
):
    anime = queries.get_one_anime(info.anime_id).anime[0]

    try:
        if not user_data:
            raise UserNotLoggedInError
        new_watchlist = watchlist.create(info=anime, email=user_data["email"])
        return new_watchlist
    except UserNotLoggedInError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="User not logged in",
        )
    except DuplicateWatchlistItem:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Anime already in watchlist",
        )


@router.delete("/api/watchlist", response_model=bool)
def delete_watchlist_item(
    info: WatchlistItemIn,
    watchlist: WatchlistQueries = Depends(),
    user_data: AccountOut = Depends(
        authenticator.try_get_current_account_data
    ),
) -> bool:
    return watchlist.delete(user_data["email"], info)
