data = {
    "anime": [
        {
            "anime_id": 5122,
            "small_image":
                "https://cdn.myanimelist.net/images/anime/1792/136743.jpg",
            "large_image":
                "https://cdn.myanimelist.net/images/anime/1792/136743l.jpg",
            "title": {
                "english":
                    "Ookami to Koushinryou: Merchant Meets the Wise Wolf",
                "japanese": "狼と香辛料",
            },
            "type": "TV",
            "episodes": None,
            "status": "Not yet aired",
            "duration": "Unknown",
            "rating": None,
            "score": None,
            "scored_by": None,
            "rank": None,
            "synopsis": "New anime project for Ookami to Koushinryou.",
            "season": None,
            "year": None,
            "studio": {"studio_id": 911, "type": "anime", "name": "Passione"},
            "genre": [
                {"id": 2, "name": "Adventure"},
                {"id": 8, "name": "Drama"},
                {"id": 10, "name": "Fantasy"},
                {"id": 22, "name": "Romance"},
            ],
        }
    ]
}


watchlist = {
    "watchlist": [
        {
            "user_email": "string",
            "anime": {
                "anime_id": 0,
                "small_image": "string",
                "large_image": "string",
                "title": {"english": "string", "japanese": "string"},
                "type": "string",
                "episodes": 0,
                "status": "string",
                "duration": "string",
                "rating": "string",
                "score": 0,
                "scored_by": 0,
                "rank": 0,
                "synopsis": "string",
                "season": "string",
                "year": 0,
                "studio": {
                    "studio_id": 0,
                    "type": "string",
                    "name": "string",
                },
                "genre": [{"id": 0, "name": "string"}],
            },
        }
    ]
}

watchlistItemData = {
    "user_email": "string",
    "anime": {
        "anime_id": 0,
        "small_image": None,
        "large_image": None,
        "title": {"english": "english", "japanese": "japanese"},
        "type": None,
        "episodes": None,
        "status": None,
        "duration": None,
        "rating": None,
        "score": None,
        "scored_by": None,
        "rank": None,
        "synopsis": None,
        "season": None,
        "year": None,
        "studio": {
            "studio_id": None,
            "type": None,
            "name": None,
        },
        "genre": [{"id": None, "name": None}],
    },
}
