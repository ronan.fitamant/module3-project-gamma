from fastapi.testclient import TestClient
from main import app
from queries.anime import AnimeQueries, GenreQueries
from sample_query_data import data
from authenticator import authenticator


client = TestClient(app)


def fake_get_current_account_data():
    return {"id": "1337", "name": "fake-user"}


class fakeGenreQueries:
    def get_genres(self):
        return {"genres": [{"id": 1, "name": "Action"}]}


class fakeAnimeQueries:
    def list_anime(self, params=None):
        return data

    def get_one_anime(self, anime_id: int):
        return data

    def get_random_anime(self, params=None):
        return data


def test_get_anime():
    app.dependency_overrides[AnimeQueries] = fakeAnimeQueries

    response = client.get("/api/anime")
    response_data = response.json()
    assert response.status_code == 200
    assert response_data == data


def test_get_one_anime():
    app.dependency_overrides[AnimeQueries] = fakeAnimeQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    response = client.get("/api/anime/get/51122")
    response_data = response.json()
    assert response.status_code == 200
    assert response_data == data


def test_get_random_anime():
    app.dependency_overrides[AnimeQueries] = fakeAnimeQueries
    response = client.get("/api/anime/random")
    response_data = response.json()
    assert response.status_code == 200
    assert response_data == data


def test_get_genres_anime():
    app.dependency_overrides[GenreQueries] = fakeGenreQueries
    response = client.get("/api/anime/genres")
    response_data = response.json()
    assert response.status_code == 200
    assert response_data == {"genres": [{"id": 1, "name": "Action"}]}
