from pydantic import BaseModel
from typing import List, Optional
import time
import requests
import random
import math


class AnimeIn(BaseModel):
    name: str
    id: int


class Titles(BaseModel):
    english: str
    japanese: Optional[str]


class Studio(BaseModel):
    studio_id: Optional[int]
    type: Optional[str]
    name: Optional[str]


class Genre(BaseModel):
    id: Optional[int]
    name: Optional[str]


class GenreList(BaseModel):
    genres: List[Genre]


class AnimeOut(BaseModel):
    anime_id: int
    small_image: Optional[str]
    large_image: Optional[str]
    title: Titles
    type: Optional[str]
    episodes: Optional[int]
    status: Optional[str]
    duration: Optional[str]
    rating: Optional[str]
    score: Optional[float]
    scored_by: Optional[int]
    rank: Optional[int]
    synopsis: Optional[str]
    season: Optional[str]
    year: Optional[int]
    studio: Studio
    genre: List[Genre]


class AnimeList(BaseModel):
    anime: List[AnimeOut]


class AnimeQueries:
    def studio_from_data(self, data: dict):
        result = {}
        if len(data.get("studios")) > 0:
            result["studio_id"] = data["studios"][0]["mal_id"]
            result["type"] = data["studios"][0]["type"]
            result["name"] = data["studios"][0]["name"]
        return result

    def create_anime_out(self, data: dict):
        studio = Studio(**self.studio_from_data(data))
        result = AnimeOut(
            anime_id=data["mal_id"],
            small_image=data["images"]["jpg"]["image_url"],
            large_image=data["images"]["jpg"]["large_image_url"],
            type=data["type"],
            title=Titles(
                english=data["title"],
                japanese=data.get("title_japanese"),
            ),
            episodes=data["episodes"],
            status=data["status"],
            duration=data["duration"],
            rating=data["rating"],
            score=data["score"],
            scored_by=data["scored_by"],
            rank=data["rank"],
            synopsis=data["synopsis"],
            season=data["season"],
            year=data["year"],
            studio=studio,
            genre=[
                Genre(id=genre["mal_id"], name=genre["name"])
                for genre in data["genres"]
            ],
        )
        return result

    def anime_response(self, params=None):
        url = "https://api.jikan.moe/v4/anime?sfw"
        res = requests.get(url, params=params)
        return res.json()

    def list_anime(self, params=None):
        data = self.anime_response(params)
        result = AnimeList(anime=[])
        for anime in data["data"]:
            result.anime.append(self.create_anime_out(anime))
        return result

    def get_one_anime(self, anime_id: int):
        url = f"https://api.jikan.moe/v4/anime/{anime_id}"
        res = requests.get(url)
        data = res.json()
        result = AnimeList(anime=[])
        result.anime.append(self.create_anime_out(data["data"]))
        return result

    def get_random_anime(self, params=None):
        params = params if params else {}
        params["min_score"] = (
            params["min_score"] if params.get("min_score") else 1
        )

        result = AnimeList(anime=[])
        data = self.anime_response(params)
        number_of_anime = data["pagination"]["items"]["total"]
        if number_of_anime < 1:
            return result
        random_anime = random.randrange(number_of_anime)
        page_number = math.floor(random_anime / 25)
        params["page"] = page_number + 1

        data = self.anime_response(params)
        index = random_anime % 25
        result.anime.append(self.create_anime_out(data["data"][index]))
        return result

    def get_ten_anime(self, params: None):
        result = AnimeList(anime=[])
        for i in range(5):
            time.sleep(0.05)
            result.anime.append(self.get_random_anime(params).anime[0])
        return result


class GenreQueries:
    def get_genres(self) -> GenreList:
        url = "https://api.jikan.moe/v4/genres/anime?filter=genres"
        res = requests.get(url)
        data = res.json()["data"]
        result = GenreList(
            genres=[
                Genre(id=genre["mal_id"], name=genre["name"]) for genre in data
            ]
        )
        return result
