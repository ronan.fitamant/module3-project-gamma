# RANDIME

Randime is a web application that allows users to explore, discover, and randomize anime suggestions. With Randime, you can find a random anime to watch, add it to your watchlist, and access detailed information about each anime. The project is built using Docker, FastAPI, RESTful API, JSON, Python, JavaScript, React, Redux, MongoDB, and CSS.

## Installation

To run Randime on your local machine, make sure you have [Docker](https://www.docker.com/) installed. Then, follow these steps:

1. Clone this repository to your local machine.
2. Open a terminal or command prompt and navigate to the project directory.
3. Create a Docker volume to persist MongoDB data:

```
docker volume create randime_mongo_data
```

4. Run the following command to build and start the Docker containers:

```
docker-compose up --build
```

5. Once the containers are up and running, you can access the Randime web application at `localhost:3000` in your web browser.

## Usage

Randime provides several features to enhance your anime-watching experience:

- **Random Anime**: From the main page, you can easily randomize an anime to watch. Click the "Randomize" button, and Randime will suggest a random anime for you.

- **Anime Filter**: Navigate to the "Anime" page from the navigation bar to explore a list of anime. You can apply filters and search criteria to find an anime that matches your preferences.

- **Sign Up & Login**: If you wish to access additional features like creating a watchlist, you can sign up for an account. Use the "Sign Up" page to create a new account, or log in if you already have one.

- **Watchlist**: Once you are logged in, you can view your watchlist on the "Watchlist" page. Add any anime that appears on the screen to your watchlist by clicking the "Add to Watchlist" button.

- **Anime Details**: Select an anime from the main page, the random page, or the anime filter page to view its full details. You'll find comprehensive information about the selected anime, helping you decide if it's the right choice for you.

## Technologies Used

- Docker
- FastAPI
- RESTful API
- JSON
- Python
- JavaScript
- React
- Redux
- MongoDB
- CSS

## Our Team

- John Coleman
  https://www.linkedin.com/in/colemanja/
  https://gitlab.com/JohnC1992

- Ronan Fitamant
  https://www.linkedin.com/in/ronan-fitamant/
  https://gitlab.com/ronan.fitamant

- Jeremiah Gonzalez
  https://www.linkedin.com/in/jeremiah-gonzalez/
  https://gitlab.com/jeremiah5799

- Austin Sekel
  https://www.linkedin.com/in/9n2v7f/
  https://gitlab.com/giants38914
