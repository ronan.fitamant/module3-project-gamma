### Main Page

## Signin

* Endpoint path: /token
* Endpoint method: POST

* Request shape (form):
  * username: string
  * password: string
  * confirm password: string
  * email: string

* Response: Account information and a token
* Response shape (JSON):
    ```json
    {
      "account": {
        "username": string,
      },
      "token": string
    }
    ```

## Login

* Endpoint path: /token
* Endpoint method: POST

* Request shape (form):
  * username: string
  * password: string

* Response: Account information and a token
* Response shape (JSON):
    ```json
    {
      "account": {
        "username": string,
      },
      "token": string
    }
    ```

## Log out

* Endpoint path: /token
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: Always true
* Response shape (JSON):
    ```json
    true
    ```

## List Anime

* Endpoint path: /api/anime/
* Endpoint method: GET

* Response: list of anime
* Response shape (JSON):
    ```json
    {
        "anime": [
            {
                "id": 0,
                "title": "string",
                "image": "string",
                "description": "string",
                "genre": "string",
                "season": 0,
                "episodes": 0,
                "score": 0
            }
        ]
    }
    ```

## Detail Anime

* Endpoint path: /api/anime/{anime_id}/
* Endpoint method: GET

* Response: Details of anime
* Response shape (JSON):
    ```json
    {
        "anime": [
            {
                "id": 0,
                "title": "string",
                "image": "string",
                "description": "string",
                "genre": "string",
                "season": 0,
                "episodes": 0,
                "score": 0,
                "type": "string",
                "status": "string",
                "aired_start": "string",
                "aired_end": "string",
                "producers": "string",
                "studios": "string",
                "source": "string",
                "genres": "string",
                "theme": "string",
                "demographic": "string",
                "duration": 0,
                "rating": "string"
            }
        ]
    }
    ```


## Watchlist

* Endpoint path: /api/watchlist/
* Endpoint method: GET

* Headers:
  * Authorization: Bearer token

* Response: list anime added to watchlist
* Response shape (JSON):
    ```json
    {
        "anime": [
            {
                "id": 0,
                "title": "string",
                "image": "string",
                "description": "string",
                "genre": "string",
                "season": 0,
                "episodes": 0,
                "score": 0
            }
        ]
    }
    ```


* Endpoint path: /api/watchlist/
* Endpoint method: POST

* Headers:
  * Authorization: Bearer token

* Response: Add anime to watchlist
* Response shape (JSON):
    ```json
    {
        "anime": [
            {
                "user_id": 0,
                "anime_id": 0,
            }
        ]
    }
    ```


* Endpoint path: /api/watchlist/{anime_id}
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: delete anime from watchlist
* Response shape (JSON):
    ```json
    true
    ```
